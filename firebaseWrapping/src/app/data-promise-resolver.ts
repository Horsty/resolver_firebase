import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DataDaoService } from './data-dao.service';

@Injectable()
export class DataPromiseResolve implements Resolve<any> {
  constructor(private DataService: DataDaoService) {}

  resolve() {
    return this.DataService.getPromiseData();
  }
}
