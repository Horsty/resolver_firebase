export interface IPromise {
  label: string;
  value: string;
}
