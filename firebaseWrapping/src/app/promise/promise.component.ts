import { Component, OnInit } from '@angular/core';
import { DataDaoService } from '../data-dao.service';
import { IPromise } from './promise';

@Component({
  selector: 'app-promise',
  templateUrl: './promise.component.html'
})
export class PromiseComponent implements OnInit {
  myPromise: IPromise = { label: '', value: '' };

  constructor(private dataService: DataDaoService) {}

  ngOnInit() {
    this.myPromise.label = this.dataService.data[0];
    this.myPromise.label = this.dataService.data[1];
  }
}
