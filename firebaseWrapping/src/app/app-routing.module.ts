import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DataPromiseResolve } from './data-promise-resolver';
import { DataObservableResolve } from './data-observable-resolver';
import { DataDaoService } from './data-dao.service';
import { ObservableComponent } from './observable/observable.component';
import { PromiseComponent } from './promise/promise.component';

const routes: Routes = [
  {
    path: 'promise',
    pathMatch: 'full',
    resolve: {
      data: DataPromiseResolve
    },
    component: PromiseComponent
  },
  {
    path: 'observable',
    pathMatch: 'full',
    resolve: {
      data: DataObservableResolve
    },
    component: ObservableComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/observable',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [DataObservableResolve, DataPromiseResolve, DataDaoService]
})
export class AppRoutingModule {}
