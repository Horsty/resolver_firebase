import { Component } from '@angular/core';
import { DataDaoService } from './data-dao.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor() {}
}
