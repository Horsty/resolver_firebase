export interface IObservable {
  label: string;
  value: string;
}
