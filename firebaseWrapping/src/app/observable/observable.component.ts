import { Component, OnInit } from '@angular/core';
import { DataDaoService } from '../data-dao.service';
import { IObservable } from './observable';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html'
})
export class ObservableComponent implements OnInit {
  myObservable: IObservable = { label : '', value : '' };

  constructor(private dataService: DataDaoService) {}

  ngOnInit() {
    this.myObservable.label = this.dataService.data[0];
    this.myObservable.label = this.dataService.data[1];
  }
}
