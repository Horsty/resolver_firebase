import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';

/** Module Firebase **/
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

/** Custom Component **/
import { PromiseComponent } from './promise/promise.component';
import { ObservableComponent } from './observable/observable.component';

const imports: any = [
  BrowserModule,
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AppRoutingModule
];
@NgModule({
  declarations: [AppComponent, PromiseComponent, ObservableComponent],
  imports: [imports],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
