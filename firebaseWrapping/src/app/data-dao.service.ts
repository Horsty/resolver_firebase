import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DataDaoService {
  private _data;

  get data(): any {
    return this._data;
  }
  constructor(private db: AngularFireDatabase) {}

  getPromiseData(): Promise<any> {
    const myPromise = new Promise((resolve) => {
      const myObservable = this.db.list('/data/promise').valueChanges();
      myObservable.subscribe(res => {
        this._data = res;
        resolve(res);
      });
    });
    return myPromise;
  }

  getObservableData(): Observable<any> {
    const mySubject = new Subject();
    const myObservable = this.db.list('/data/observable').valueChanges();
    myObservable.subscribe(res => {
      this._data = res;
      mySubject.next(res);
      mySubject.complete();
    });
    return mySubject.asObservable();
  }
}
