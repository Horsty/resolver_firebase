export const environment = {
  production: true,
  firebase: {
    apiKey: 'myApiKey',
    authDomain: 'myAuthDomain', /** generaly ending by .firebaseapp.com **/
    databaseURL: 'myDatabaseUrl', /** generaly like this https://my-project.firebaseio.com **/
    projectId: 'myProjectId',
    storageBucket: '',
    messagingSenderId: '*******'
  }
};
