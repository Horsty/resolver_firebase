# Français
## Objectif
 J'ai développé un petit exemple simpliste d'une application angular qui utiliserai Firebase pour fonctionner.
 Je me suis retrouvé dans un cas, ou j'ai eut besoin d'utiliser la méthode `this.db.list('/data').valueChanges()` ainsi que des resolvers et la c'est le drâme!
 La méthode ne retourne pas d'observable, avec un peu d'aide et de recheche, voici une solution qui permet d'utiliser les resolvers.
 Le principe est de wrapper le retour dans une résolution asynchrone. Au choix via une promesse (solution que je préfére) soit via une observable.
 En espérant que cela vous aides ;)
 
## Commencer
 Pour utiliser ce que j'ai mis en place, il faut posséder des bases dans:
  - Angular
  - Les observables
  - Les promesses
  - Firebase et la base en temps réel
  - Un projet angular actif connecté à firebase
  - Firebase d'installé (lancé la commande: `npm i angularfire2`)
   
## Reste à faire
 - Commenter le code
 - Améliorer l'explication de la solution
 - Ajout le json de la base en exemple
 - Corriger les fautes d'orthographes :p 

# English
## Objectif
 I develop this little exemple of an angular app, to helping people who want's to create his own app using resolver with firebase.
 I met a little problem with `this.db.list('/data').valueChanges()`. 
 This method, to get my data on my firebase database, doesn't return an Observable so resolver can't works as well.
 The solution here that is to wrapping the return, wait for it, and return an observable when it's ok.
 I develop two solution, with promise (i prefer in this case), and an other with observable.
 I hope it's help you ;) 
 
## Get Started
### Prerequisite
 - Knowledge of :
  - Angular
  - Observable
  - Promise
  - Firebase
 - An actif angular's project
 - AngularFireDatabase installed ( run this command: `npm i angularfire2`)
 
## Remainder
 - Comment function in code
 - Explain the principe
 - Add json exemple of database
 - Correct spelling mistakes: p


 